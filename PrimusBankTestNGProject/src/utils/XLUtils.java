package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XLUtils 
{
  public static  FileInputStream fis;
  public static FileOutputStream fos;
  public static Workbook wb;
  public static Sheet ws;
  public static Row row;
  public static Cell cell;
  public static CellStyle style;
	
  //To Count Row
	public static int getRowCount(String xlfile, String xlsheet) throws IOException
	{
		fis = new FileInputStream(xlfile);
		wb = new XSSFWorkbook(fis);
	    ws = wb.getSheet(xlsheet);
		int rowcount = ws.getLastRowNum();
		wb.close();
		fis.close();
		return rowcount;
	}
	
	//To count Column
	public static short getColumnCount(String xlfile,String xlsheet,int rownum) throws IOException
	{
		
		fis = new FileInputStream(xlfile);
		wb = new XSSFWorkbook(fis);
	    ws = wb.getSheet(xlsheet);
		row= ws.getRow(rownum);
		short colcount= row.getLastCellNum();
		wb.close();
		fis.close();		
		return colcount;		
	}
	
	//To Read String cell data
	public static String getStringCellData(String xlfile,String xlsheet,int rownum,int colnum) throws IOException
	{
		fis = new FileInputStream(xlfile);
		wb = new XSSFWorkbook(fis);
	    ws = wb.getSheet(xlsheet);
		row= ws.getRow(rownum);
		String data;
		try 
		{
			cell=row.getCell(colnum);
			data=cell.getStringCellValue();
		} catch (Exception e) 
		{
			data="";
		}
		wb.close();
		fis.close();
		return data;
		
	}
	
	//To read Numeric Cell Data
	public static double getNumericCellData(String xlfile,String xlsheet,int rownum,int colnum) throws IOException
	{
		fis = new FileInputStream(xlfile);
		wb = new XSSFWorkbook(fis);
	    ws = wb.getSheet(xlsheet);
		row= ws.getRow(rownum);
		double data;
		try 
		{
			cell=row.getCell(colnum);
			data=cell.getNumericCellValue();
		} catch (Exception e) 
		{
			data=0;
		}
		wb.close();
		fis.close();
		return data;
		
	}
	
	//To read Boolean Cell Data
	public static boolean getBooleanCellData(String xlfile,String xlsheet,int rownum,int colnum) throws IOException
	{
		fis = new FileInputStream(xlfile);
		wb = new XSSFWorkbook(fis);
	    ws = wb.getSheet(xlsheet);
		row= ws.getRow(rownum);
		boolean data;
		try 
		{
			cell=row.getCell(colnum);
			data=cell.getBooleanCellValue();
		} catch (Exception e) 
		{
			data=false;
		}
		wb.close();
		fis.close();
		return data;
		
	}
	
	
	//To Write Data into Cell
	public static void setCellData(String inputxlfile,String xlsheet,int rownum,int colnum,String data,String outputxlfile) throws IOException
	{
		fis = new FileInputStream(inputxlfile);
		wb = new XSSFWorkbook(fis);
	    ws = wb.getSheet(xlsheet);
		row= ws.getRow(rownum);
		cell=row.createCell(colnum);
		cell.setCellValue(data);
		fos=new FileOutputStream(outputxlfile);
		wb.write(fos);
		wb.close();
		fis.close();
		fos.close();
		
	}
	
	//Set a green Color
	public static void fillGreenColor(String xlfile,String xlsheet,int rownum,int colnum) throws IOException
	{
		
		fis = new FileInputStream(xlfile);
		wb = new XSSFWorkbook(fis);
	    ws = wb.getSheet(xlsheet);
		row= ws.getRow(rownum);
		cell=row.getCell(colnum);
		
		style=wb.createCellStyle();
		style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		cell.setCellStyle(style);
		fos=new FileOutputStream(xlfile);
		wb.write(fos);
		wb.close();
		fis.close();
		fos.close();
	}
	
	//set a red color
	public static void fillRedColor(String xlfile,String xlsheet,int rownum,int colnum) throws IOException
	{
		
		fis = new FileInputStream(xlfile);
		wb = new XSSFWorkbook(fis);
	    ws = wb.getSheet(xlsheet);
		row= ws.getRow(rownum);
		cell=row.getCell(colnum);
		
		style=wb.createCellStyle();
		style.setFillForegroundColor(IndexedColors.RED.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		
		cell.setCellStyle(style);
		fos=new FileOutputStream(xlfile);
		wb.write(fos);
		wb.close();
		fis.close();
		fos.close();
	}
	
	
}
