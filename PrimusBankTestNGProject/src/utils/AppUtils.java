package utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class AppUtils {
	public static WebDriver driver;
	public static String url="http://primusbank.qedgetech.com";
	
	@BeforeTest
	public static void launchApp() {
		
		System.setProperty("webdriver.chrome.driver", "E:\\Automation Project\\Selenium\\PrimusBank\\src\\utils\\Drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(50,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
	
		driver.get(url);
		
	}
	
	@AfterTest
	public static void closeApp() {
		driver.close();
	}

}
