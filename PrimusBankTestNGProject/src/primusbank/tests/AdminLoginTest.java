package primusbank.tests;

import primusbank.library.PrimusBankHomePage;
import utils.AppUtils;

public class AdminLoginTest extends AppUtils{

	public static void main(String[] args){
		
		launchApp();
		
		PrimusBankHomePage phom=new PrimusBankHomePage();
		boolean res=phom.adminLogin("Admin","Admin");
		if(res) {
			System.out.println("Admin Login Test Pass");
		}else {
			System.out.println("Admin Login Test Fail");
		}
		
		
		phom.adminLogout();
		AppUtils.closeApp();
	}

}
