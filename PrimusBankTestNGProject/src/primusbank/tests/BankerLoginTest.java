package primusbank.tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import primusbank.library.PrimusBankHomePage;
import utils.AppUtils;
import utils.XLUtils;

public class BankerLoginTest extends AppUtils{

	String datafile="E:\\Automation Project\\Selenium\\PrimusBank\\TestData\\Data.xlsx";
	String datasheet="BankerLoginData";

	@Test
	public void bankerLogin() throws IOException{
		
		String bname,uid,pass;
		boolean res;
		
		PrimusBankHomePage phome=new PrimusBankHomePage();
		int rc=XLUtils.getRowCount(datafile, datasheet);
		
		for(int i=1; i<=rc; i++) {
			bname=	XLUtils.getStringCellData(datafile, datasheet, i, 0);
			uid= XLUtils.getStringCellData(datafile, datasheet, i, 1);
			pass=XLUtils.getStringCellData(datafile, datasheet, i, 2);

			res=phome.bankerLogin(bname,uid,pass);

			if(res) {
				
				XLUtils.setCellData(datafile, datasheet, i, 3, "Pass", datafile);
				XLUtils.fillGreenColor(datafile, datasheet, i, 3);

			}else {
				XLUtils.setCellData(datafile, datasheet, i, 3, "Fail", datafile);
				XLUtils.fillRedColor(datafile, datasheet, i, 3);
			}
			
			phome.bankerLogout();
			
		}

		

	}
}
