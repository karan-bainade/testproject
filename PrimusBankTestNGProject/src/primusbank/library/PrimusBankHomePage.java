package primusbank.library;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import utils.AppUtils;

public class PrimusBankHomePage  extends AppUtils{

	//Admin Login

	public boolean adminLogin(String uid,String pwd)
	{
		String expurl,acturl;
		expurl="adminflow";
		driver.findElement(By.id("txtuId")).sendKeys(uid);
		driver.findElement(By.id("txtPword")).sendKeys(pwd);
		driver.findElement(By.id("login")).click();
		acturl=driver.getCurrentUrl();
		if(acturl.toLowerCase().contains(expurl.toLowerCase()))
		{
			return true;
		}else
		{
			return false;
		}
	}

	public void adminLogout()
	{
		driver.findElement(By.xpath("//img[@src='images/admin_but_03.jpg']")).click();
	}
	
	
//----------------------------------------------------------------------------------
	public boolean bankerLogin(String brname,String uname,String pword)
	{
		String expurl,acturl;
		expurl="bankers_flow";

		Select blist=new Select(driver.findElement(By.id("drlist")));
		blist.selectByVisibleText(brname);
		driver.findElement(By.id("txtuId")).sendKeys(uname);
		driver.findElement(By.id("txtPword")).sendKeys(pword);
		driver.findElement(By.id("login")).click();
		acturl=driver.getCurrentUrl();
		if(acturl.toLowerCase().contains(expurl.toLowerCase()))
		{
			return true;
		}else
		{
			return false;
		}	
	}

	public boolean bankerLogout()
	{
		driver.findElement(By.id("IMG1")).click();
		if(driver.findElement(By.id("login")).isDisplayed())
		{
			return true;
		}else
		{
			return false;
		}
	}
//--------------------------------------------------------------------------------


}
